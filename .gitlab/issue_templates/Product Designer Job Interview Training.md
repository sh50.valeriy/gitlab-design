### Key steps :walking: 

* [ ] `insert_trainee_username` ask the person that will lead the first interview you’ll shadow to schedule an intro call with you.
* [ ] After the 1st shadowed interview add a new discussion in this issue, titled `1st shadow thoughts and comments`. Both participants should add their thoughts and comments about the interview they did together. What did they observe, notice, and learn?
* [ ] After the 2nd shadow add a new discussion in this issue, titled `2nd shadow thoughts and comments`. Again, add your comments and throughts.
* [ ] `insert_trainee_username` create their own template with a list of questions for job interviews.
* [ ] `insert_trainee_username`’s 1st lead interview. Add a new discussion in this issue, titled `1st lead thoughts and comments`. Discuss your experience and learnings with the person who shadowed you.

### General tips and guidance :bulb: 

#### Before the interview:
* Review the questions you'll ask during the interview (Greenhouse)
* Review the goals and needs of the Group the candidate is being interviewed for by reviewing the kickoff issue or reaching out to the Group's Product Design Manager
* Be familiar with the answers we're looking for in each question:
    *  Senior Product Designer - [Google doc](https://docs.google.com/document/d/1LLWyEmegVh1m78r4UMTdgGpgB1fpCCqQ6dB6WASBNAw/edit?usp=sharing)
    *  Intermediate Product Designer - [Google doc](https://docs.google.com/document/d/1EjD-G1KUADLeoGpPEOs2e19_oNeDnL8jC95UMSUo864/edit?usp=sharing)
    * Recommended to have the appropriate (i.e. Senior or Intermediate) google doc rubric open when filling in the Greenhouse scorecard as a reference to assist with overall candidate scoring
* We want to ask the same questions to every candidate
* Check candidate's portfolio, resumé, and cover letter

#### During the interview:
* Be warm, chatty and nice. Explain how the interviews work at GitLab.
* Start off with warmup questions to get the candidate talking (provided in Greenhouse)
* If you can, start discussions with the candidate based on their answers instead of just going from one question to another. This way, you'll get answers with deeper insights. Ask follow-up questions if needed.
    * However, this should be balanced with our goal of getting through each of the provided questions in Greenhouse, in the order they're listed, for consistency.

#### After the interview:
* Fill out the scorecard in Greenhouse. Be sure to use the [rubric](https://docs.google.com/document/d/1OqEZbzb9s5ose_cEmVKBeVg27vElCPNAZ360T0rBq30/edit?usp=sharing) and [overall scoring definitions](https://about.gitlab.com/handbook/hiring/conducting-a-gitlab-interview/#engineering-division)
If you're on the fence about a candidate, think about these:
  * Would this candidate fit the team? Are they humble, enthusiastic, collaborative?
  * How interested are they in the role at GitLab? How well did they research the company? Read the handbook? What GitLab value resonates with them?
  * Does the candidate bring something new to the team? A new skill or unique experience that is relevant?
  * Is there a basic alignment with the open position? Are more technical skills required, for example?

### STAR questions :star: 

STAR questions are the key part of a job interview. It is possible to decline candidates who performed ok on other questions but didn’t perform at all on the STAR questions. Here's why:

* STAR questions ask about their past behaviour which is an indicator of how the candidate will behave in similar future situations
* they are a very good indicator whether the candidate has the relevant experience that you're asking them about
* they may reveal a lot about the candidate's personality
* it reduces the need to rely on your gut feeling
* we have a wide range to pick from so they should cover any concerns you might have about the candidate

_How should a candidate respond to a STAR question?_ :thinking: 

The candidate's answer needs to be a concrete example. These questions usually start with "Tell me about a time when you..." So they're asking for specific examples. If the candidate gives you an answer that goes something like: "Well, I generally do..." you need to follow-up with: "Ok, can you give me a recent example?"

If they're not giving a specific example, the likelihood is that they don't have experience with what you're asking them about. So they try to come up with a satisfactory generic answer. But only specific and concrete examples count. If the candidate fails to provide one, they are not successful on this question.

### Criteria for evaluating candidates :chart_with_upwards_trend: 

* Portfolio
  * How strong are their case studies?
  * Are you able to understand the work they did just by reading through the case studies?
* Resumé
  * Are they a potential job hopper (do they have many tenures that are shorter than two years)?
  * Are they fully transparent about their past tenures?
* Cover letter
  * Is there one?
  * Is it written specifically for GitLab and do they explain why they’re applying? 
  * Are they linking relevant case studies that you should review?
* Soft skills
  * Communication — how well can they explain their work? How easy is it to discuss it with them?
  * What is the candidate like as a person? Would they fit the team and GitLab’s culture?
* Hard skills
  * Product design skills
  * Visual design
  * UX research
  * Design systems experience
  * Relevant role-specific experience
  * Are they capable of thinking big but shipping small?
* What questions do they ask you? Just two basic questions or 4-5 elaborate and interesting ones? The questions they ask can also be a good indicator of their level of interest in GitLab and the job opportunity.
